
export default class Vector {

    constructor(x, y)  {
        this.x = x;
        this.y = y;
    }

    get length() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    add(v) {
        return new Vector(this.x + v.x, this.y + v.y);
    }

    sub(v) {
        return new Vector(this.x - v.x, this.y - v.y);
    }

    mult(s) {
        return new Vector(this.x * s, this.y * s);
    }

    div(s) {
        return new Vector(this.x / s, this.y / s);
    }

    static distance (v0, v1) {
        var dx = v0.x - v1.x;
        var dy = v0.y - v1.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

}


