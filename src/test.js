
import Vector from "./vector";

const v0 = new Vector(2, 5);
const v1 = new Vector(4, 2);

console.log(v0.add(v1));
console.log(v0.sub(v1));
console.log(v0.mult(10));
console.log(v0.div(2));

