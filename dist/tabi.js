"use strict";

// ES6記法ではないJavaScriptファイル

$(function () {
    if ($(".page-index").length > 0) {
        var CandidateType;
        var vue;

        (function () {
            var Candidate = function Candidate(opt) {
                opt = opt || {};
                this.type = opt.type;
                this.label = opt.label;
                this.sub = opt.sub;
                this.count = opt.count;
                this.link = opt.link;
                this.selected = false;
            };

            CandidateType = {
                Area: 0,
                Category: 1
            };
            vue = new Vue({
                el: ".page-index",
                data: {

                    // view側で参照するためにdata propertyに設定
                    CandidateType: CandidateType,

                    ready: true,
                    placeholder: "旅行先",
                    keyword: "",
                    selected: -1,
                    elements: [],
                    candidates: [],
                    req: null,
                    showRecommend: false,
                    showCandidates: false
                },
                watch: {
                    "keyword": function keyword(cur, prev) {
                        this.updateKeyword(cur);
                    }
                },
                ready: function ready() {
                    this.requestArea();
                },
                methods: {
                    buildDictionary: function buildDictionary(str) {
                        if (Utils.isKatakana(str)) {
                            return [str, Utils.katakanaToHiragana(str), Utils.zenkanaToHankana(str)];
                        }
                        return [str];
                    },
                    requestArea: function requestArea() {
                        $.ajax({
                            url: "/api/area/get_area_data_list_with_tour_count",
                            dataType: "JSON",
                            success: function (data) {
                                data.forEach(function (d) {
                                    var dict = this.buildDictionary(d.tour_area_name);
                                    if (d.dictionary) dict = dict.concat(d.dictionary);

                                    var cand = new Candidate({
                                        type: CandidateType.Area,
                                        label: d.tour_area_name,
                                        count: d.tour_count,
                                        link: "/tour_list/area/" + d.tour_area_id
                                    });
                                    cand.dict = dict;

                                    this.$data.elements.push(cand);
                                }.bind(this));
                            }.bind(this)
                        });
                    },
                    requestCategory: function requestCategory(query) {
                        // 前回のrequestが残っている場合はキャンセル(abort)する
                        if (this.$data.req != null) this.$data.req.abort();

                        this.$data.req = $.ajax({
                            url: "/api/category/get_tour_area_category_with_area_and_tour_count",
                            method: "GET",
                            data: { query: query },
                            dataType: "JSON"
                        }).done(function (data) {
                            var categories = data.map(function (d) {
                                return new Candidate({
                                    type: CandidateType.Category,
                                    label: d.tour_area_category_name,
                                    sub: d.tour_area_name,
                                    count: d.tour_count,
                                    link: "/tour_list/area/" + d.tour_area_id + "/category/" + d.tour_area_category_tour_category_id
                                });
                            }.bind(this));
                            this.updateCandidates(query, categories);
                        }.bind(this));
                    },
                    same: function same(candidates0, candidates1) {
                        if (candidates0.length != candidates1.length) {
                            return false;
                        }
                        for (var i = 0, n = candidates0.length; i < n; i++) {
                            var cand0 = candidates0[i],
                                cand1 = candidates1[i];
                            if (cand0.type != cand1.type || cand0.label != cand1.label) return false;
                        }
                        return true;
                    },
                    updateKeyword: function updateKeyword(text) {
                        this.$data.showRecommend = text.length <= 0;
                        if (text.length >= 2) {
                            this.$data.showCandidates = true;
                            this.requestCategory(text);
                        } else {
                            this.$data.showCandidates = false;
                            this.$data.candidates = []; // clear
                            this.$data.selected = -1;
                        }
                    },
                    updateCandidates: function updateCandidates(query, categories) {
                        var areas = this.$data.elements.filter(function (elm) {
                            return elm.dict.findIndex(function (str) {
                                return str.indexOf(query) != -1;
                            }) >= 0;
                        });
                        var candidates = areas.concat(categories);
                        if (!this.same(this.$data.candidates, candidates)) {
                            this.$data.candidates = candidates;
                            this.$data.candidates.forEach(function (cand) {
                                cand.selected = false;
                            });
                            if (this.$data.candidates.length == 1) {
                                this.$data.selected = 0;
                                this.$data.candidates[0].selected = true;
                            } else {
                                this.$data.selected = -1;
                            }
                        }
                    },
                    onFocus: function onFocus() {
                        var len = this.$data.keyword.length;
                        if (len <= 0) {
                            this.$data.showRecommend = true;
                        } else if (len >= 2) {
                            this.$data.showCandidates = true;
                        }
                        this.$data.placeholder = "行き先を入力して下さい";
                    },
                    onBlur: function onBlur(e) {
                        this.$data.placeholder = "旅行先";
                    },
                    onKeyDown: function onKeyDown(e) {
                        if (e.keyCode == 13) {
                            this.search();
                            return;
                        }
                        if (this.$data.showCandidates) {
                            var cur = this.$data.selected;
                            if (e.keyCode == 38 && cur >= 1) {
                                // arrow up
                                this.unselect(cur);
                                this.select(cur - 1);
                            } else if (e.keyCode == 40 && cur < this.$data.candidates.length - 1) {
                                // arrow down
                                this.unselect(cur);
                                this.select(cur + 1);
                            }
                        }
                    },
                    onClick: function onClick(e) {
                        e.stopPropagation();
                    },
                    onSubmit: function onSubmit(e) {
                        if (this.$data.keyword.length > 0) {
                            this.search();
                        }
                        e.stopPropagation();
                    },
                    onTouchEnd: function onTouchEnd(e) {},
                    select: function select(index) {
                        if (index >= 0 && index < this.$data.candidates.length) {
                            this.$data.candidates[index].selected = true;
                            this.$data.selected = index;
                        }
                    },
                    unselect: function unselect(index) {
                        if (index >= 0 && index < this.$data.candidates.length) {
                            this.$data.candidates[index].selected = false;
                        }
                    },
                    search: function search() {
                        var cur = this.$data.selected;
                        if (cur < 0) {
                            // エリアが選択されずにEnterされた場合、検索ページに遷移するようにする
                            var keyword = this.$data.keyword;
                            window.location.href = "/tour_list/search?keyword=" + keyword;
                        } else if (cur >= 0 && cur < this.$data.candidates.length) {
                            // enter to select
                            var cand = this.candidates[cur];
                            window.location.href = cand.link;
                        }
                    }
                }
            });


            $(window).on('click', function (e) {
                vue.$data.showRecommend = vue.$data.showCandidates = false;
            });
        })();
    }
});