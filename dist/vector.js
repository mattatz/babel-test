"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Vector = function () {
    function Vector(x, y) {
        _classCallCheck(this, Vector);

        this.x = x;
        this.y = y;
    }

    _createClass(Vector, [{
        key: "add",
        value: function add(v) {
            return new Vector(this.x + v.x, this.y + v.y);
        }
    }, {
        key: "sub",
        value: function sub(v) {
            return new Vector(this.x - v.x, this.y - v.y);
        }
    }, {
        key: "mult",
        value: function mult(s) {
            return new Vector(this.x * s, this.y * s);
        }
    }, {
        key: "div",
        value: function div(s) {
            return new Vector(this.x / s, this.y / s);
        }
    }, {
        key: "length",
        get: function get() {
            return Math.sqrt(this.x * this.x + this.y * this.y);
        }
    }], [{
        key: "distance",
        value: function distance(v0, v1) {
            var dx = v0.x - v1.x;
            var dy = v0.y - v1.y;
            return Math.sqrt(dx * dx + dy * dy);
        }
    }]);

    return Vector;
}();

exports.default = Vector;