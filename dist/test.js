"use strict";

var _vector = require("./vector");

var _vector2 = _interopRequireDefault(_vector);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var v0 = new _vector2.default(2, 5);
var v1 = new _vector2.default(4, 2);

console.log(v0.add(v1));
console.log(v0.sub(v1));
console.log(v0.mult(10));
console.log(v0.div(2));